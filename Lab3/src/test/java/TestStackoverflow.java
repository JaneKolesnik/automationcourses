import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import stackoverflow.FirstQuestion;
import stackoverflow.MainPage;
import stackoverflow.SignUp;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Yana on 30.05.2016.
 */
public class TestStackoverflow {

    private static WebDriver driver;
    private static MainPage mainPage;

    @BeforeClass
    public static void setUp() throws Exception {
        driver = new FirefoxDriver();
        driver.get("http://stackoverflow.com/");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        mainPage=new MainPage(driver);
    }
    @Before
    public void returnToMain() throws Exception {
        driver.get("http://stackoverflow.com/");
    }

    @AfterClass
    public static void tearDown() throws Exception {
        driver.quit();
    }

    @Test
    public void test_number_of_questions() throws InterruptedException {
        int ref_value  = 300;
        String act_value_s = mainPage.elem_indicator.getText();
        int act_valie_i = Integer.parseInt(act_value_s);
        Assert.assertTrue("Количество меньше 300", (act_valie_i>= ref_value));
    }

    @Test
    public void test_login_from_social() throws InterruptedException {
        SignUp SighnUpPage=mainPage.navigateToSignUp();

        Assert.assertTrue("Кнопки логина через Google нет",
                SighnUpPage.btn_login_byGoogle.isDisplayed());
        Assert.assertTrue("Кнопки логина через Facebook нет",
                SighnUpPage.btn_login_byFacebook.isDisplayed());
    }

    @Test
    public void test_date_of_question() throws InterruptedException {
        FirstQuestion FirstQuestionPage=mainPage.navigateToFirstQuestion();

        Assert.assertTrue("Вопрос задан не сегодня",
                FirstQuestionPage.txta_today.isDisplayed());
    }
}
