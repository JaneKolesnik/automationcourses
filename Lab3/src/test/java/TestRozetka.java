import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import rozetka.MainPage;

import java.util.concurrent.TimeUnit;

/**
 * Created by Yana on 30.05.2016.
 */
public class TestRozetka {
    private static WebDriver driver;
    private static MainPage mainPage;

    @BeforeClass
    public static void launchBrowser() throws Exception {
        driver = new FirefoxDriver();
        driver.get("http://rozetka.com.ua/");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        mainPage= new MainPage(driver);

    }
    @Before
    public void go_to_main() throws Exception {
        driver.get("http://rozetka.com.ua/");
    }

    @AfterClass
    public static void tearDown() throws Exception {
        driver.quit();
    }

    @Test
    public void test_logo_is_displayed() throws InterruptedException {
        Assert.assertTrue("Логотип Розетки отсутствует на странице",
                mainPage.elem_Rozetka.isDisplayed());
    }
    @Test
    public void test_Apple_menu_item() throws InterruptedException {
        Assert.assertTrue("Раздел меню Apple отсутствует на странице",
                mainPage.elem_menu_Apple.isDisplayed());
    }
    @Test
    public void test_MP3_text_in_menu_items() throws InterruptedException {
        Assert.assertTrue("Раздел меню, содержащий 'MP3' отсутсвует на странице",
                mainPage.elem_menu_MP3.isDisplayed());
    }
    @Test
    public void test_select_city_kiev_kharkov_odessa() throws InterruptedException {
        mainPage.btn_header_city.click();
        Assert.assertTrue("Харьков отсутсвует в поп-апе",
                mainPage.btn_cityChooser_kharkiv.isDisplayed());
        Assert.assertTrue("Киев отсутсвует в поп-апе",
                mainPage.btn_cityChooser_kyiv.isDisplayed());
        Assert.assertTrue("Одесса отсутсвует в поп-апе",
                mainPage.btn_cityChooser_odessa.isDisplayed());
    }
    @Test
    public void test_cart_is_empty() throws InterruptedException {
        mainPage.btn_header_cart.click();
        Assert.assertTrue("Корзина не пуста",
                mainPage.txta_cart_empty.isDisplayed());
    }
}
