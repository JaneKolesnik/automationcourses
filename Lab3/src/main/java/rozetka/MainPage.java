package rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Yana on 06.06.2016.
 */
public class MainPage {
    private WebDriver driver;

    public MainPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver=driver;
    }

    @FindBy (xpath = ".//*[@class='logo']")
    public WebElement elem_Rozetka;

    @FindBy (xpath = ".//*/li[@class='m-main-l-i'][contains(a,'Apple')]")
    public WebElement elem_menu_Apple;

    @FindBy (xpath = ".//*/li[@class='m-main-l-i'][contains(a,'MP3')]")
    public WebElement elem_menu_MP3;

    @FindBy (xpath = ".//*[contains(@id,'cart_block')]/a")
    public WebElement btn_header_cart;

    @FindBy (xpath = ".//*[@id='drop-block'][contains(h2,'Корзина пуста')]")
    public WebElement txta_cart_empty;

    @FindBy (xpath = ".//*[@id='city-chooser']/a")
    public WebElement btn_header_city;

    @FindBy (xpath = ".//*[@id='city-chooser']//*[@class='popup-css header-city-choose-popup']//*[contains(a,'Харьков')] | .//*[@id='city-chooser']//*[@class='popup-css header-city-choose-popup']//*[contains(span,'Харьков')]")
    public WebElement btn_cityChooser_kharkiv;

    @FindBy (xpath = ".//*[@id='city-chooser']//*[@class='popup-css header-city-choose-popup']//*[contains(a,'Киев')] | .//*[@id='city-chooser']//*[@class='popup-css header-city-choose-popup']//*[contains(span,'Киев')]")
    public WebElement btn_cityChooser_kyiv;

    @FindBy (xpath = ".//*[@id='city-chooser']//*[@class='popup-css header-city-choose-popup']//*[contains(a,'Одесса')] | .//*[@id='city-chooser']//*[@class='popup-css header-city-choose-popup']//*[contains(span,'Одесса')]")
    public WebElement btn_cityChooser_odessa;


}
