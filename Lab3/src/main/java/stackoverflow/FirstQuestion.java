package stackoverflow;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Yana on 06.06.2016.
 */
public class FirstQuestion {
    private WebDriver driver;

    public FirstQuestion(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver=driver;
    }

    @FindBy (xpath = ".//*[@id='qinfo']//*[contains(b,'today')]")
    public WebElement txta_today;
}
