package stackoverflow;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Yana on 06.06.2016.
 */
public class SignUp {

    private WebDriver driver;

    public SignUp(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy (xpath = ".//*[@id='openid-buttons']//*[contains(span,'Google')]")
    public WebElement btn_login_byGoogle;

    @FindBy (xpath = ".//*[@id='openid-buttons']//*[contains(span,'Facebook')]")
    public WebElement btn_login_byFacebook;

}
