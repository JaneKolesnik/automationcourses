package stackoverflow;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import sun.security.krb5.internal.PAForUserEnc;

/**
 * Created by Yana on 06.06.2016.
 */
public class MainPage {
    private WebDriver driver;

    public MainPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver=driver;
    }

    @FindBy(xpath = ".//*/a[@class='login-link'][text() = 'sign up']")
    public WebElement lnk_topbar_signup;

    public SignUp navigateToSignUp() {
        lnk_topbar_signup.click();
        return new SignUp(driver);
    }

    @FindBy (xpath = ".//*/a[@class='question-hyperlink']")
    public WebElement lnk_first_question;

    public FirstQuestion navigateToFirstQuestion() {
        lnk_first_question.click();
        return new FirstQuestion(driver);
    }

    @FindBy (xpath = ".//*[@id='tabs']/*/span[@class='bounty-indicator-tab']")
    public WebElement elem_indicator;

}
