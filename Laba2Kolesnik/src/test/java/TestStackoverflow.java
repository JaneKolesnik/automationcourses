import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Yana on 30.05.2016.
 */
public class TestStackoverflow {

    private static WebDriver driver;
    private static String baseUrl;

    @BeforeClass
    public static void setUp() throws Exception {
        driver = new FirefoxDriver();
        baseUrl = "http://stackoverflow.com/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }
    @Before
    public void go_to_main() throws Exception {
        driver.get(baseUrl);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        driver.quit();
    }


    @Test
    public void test_number_of_questions() throws InterruptedException {
        String amount1 = driver.findElement(By.xpath(".//*[@id='tabs']/*/span[@class='bounty-indicator-tab']")).getText();
        int amount2 = Integer.parseInt(amount1);
        int amount3  = 300;
        Assert.assertTrue("Количество меньше 300", (amount2 >= amount3));
    }

    @Test
    public void test_login_from_social() throws InterruptedException {
        driver.findElement(By.xpath(".//*/a[@class='login-link'][text() = 'sign up']")).click();
        Assert.assertTrue("Кнопки логина через Google нет",
                driver.findElement(By.xpath(".//*[@id='openid-buttons']//*[contains(span,'Google')]")).isDisplayed());
        Assert.assertTrue("Кнопки логина через Facebook нет",
                driver.findElement(By.xpath(".//*[@id='openid-buttons']//*[contains(span,'Facebook')]")).isDisplayed());
    }

    @Test
    public void test_date_of_question() throws InterruptedException {
        driver.findElement(By.xpath(".//*/a[@class='question-hyperlink']")).click();
        Assert.assertTrue("Вопрос задан не сегодня",
                driver.findElement(By.xpath(".//*[@id='qinfo']//*[contains(b,'today')]")).isDisplayed());
    }
}
