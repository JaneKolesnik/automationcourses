import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by Yana on 30.05.2016.
 */
public class TestRozetka {
    private static WebDriver driver;
    private static String baseUrl;

    @BeforeClass
    public static void launchBrowser() throws Exception {
        driver = new FirefoxDriver();
        baseUrl = "http://rozetka.com.ua/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }
    @Before
    public void go_to_main() throws Exception {
        driver.get(baseUrl);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        driver.quit();
    }

    @Test
    public void test_logo_is_displayed() throws InterruptedException {
        Assert.assertTrue("Логотип Розетки отсутствует на странице",
                driver.findElement(By.xpath(".//*[@class='logo']")).isDisplayed());
    }
    @Test
    public void test_Apple_menu_item() throws InterruptedException {
        Assert.assertTrue("Раздел меню Apple отсутствует на странице",
                driver.findElement(By.xpath(".//*/li[@class='m-main-l-i'][contains(a,'Apple')]")).isDisplayed());
    }
    @Test
    public void test_MP3_text_in_menu_items() throws InterruptedException {
        Assert.assertTrue("Раздел меню содержащий MP3 отсутсвует на странице",
                driver.findElement(By.xpath(".//*/li[@class='m-main-l-i'][contains(a,'MP3')]")).isDisplayed());
    }
    @Test
    public void test_select_city_kiev_kharkov_odessa() throws InterruptedException {
        driver.findElement(By.xpath(".//*[@id='city-chooser']/a")).click();
        Assert.assertTrue("Харьков отсутсвует в поп-апе",
                driver.findElement(By.xpath(".//*[@id='city-chooser']//*[@class='popup-css header-city-choose-popup']//*[contains(a,'Харьков')] | .//*[@id='city-chooser']//*[@class='popup-css header-city-choose-popup']//*[contains(span,'Харьков')]")).isDisplayed());
        Assert.assertTrue("Киев отсутсвует в поп-апе",
                driver.findElement(By.xpath(".//*[@id='city-chooser']//*[@class='popup-css header-city-choose-popup']//*[contains(a,'Киев')] | .//*[@id='city-chooser']//*[@class='popup-css header-city-choose-popup']//*[contains(span,'Киев')]")).isDisplayed());
        Assert.assertTrue("Одесса отсутсвует в поп-апе",
                driver.findElement(By.xpath(".//*[@id='city-chooser']//*[@class='popup-css header-city-choose-popup']//*[contains(a,'Одесса')] | .//*[@id='city-chooser']//*[@class='popup-css header-city-choose-popup']//*[contains(span,'Одесса')]")).isDisplayed());
    }
    @Test
    public void test_cart_is_empty() throws InterruptedException {
        driver.findElement(By.xpath(".//*[contains(@id,'cart_block')]/a")).click();
        Assert.assertTrue("Корзина не пуста",
                driver.findElement(By.xpath(".//*[@id='drop-block'][contains(h2,'Корзина пуста')]")).isDisplayed());
    }
}
